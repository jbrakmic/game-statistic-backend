package com.game.statistic.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Repository;

import com.game.statistic.model.Game;
import com.game.statistic.model.TelegramNotification;
import com.game.statistic.worker.GameStatisticWorker;

@Repository
public class GameRepository {

	private Map<String, List<Game>> games = new HashMap<String, List<Game>>();
	private Map<String, TelegramNotification> sentGames = new HashMap<String, TelegramNotification>();
	private Map<String, TelegramNotification> gamesToBeSend = new HashMap<String, TelegramNotification>();

	public Map<String, List<Game>> getGames() {
		return games;
	}

	public List<Game> getGameResultsForGame(String gameId) {
		return this.games.get(gameId);
	}

	public Map<String, List<Game>> getResultsForTime(String forTime) {
		Integer time = null;
		if (!forTime.contentEquals("half")) {
			Integer.valueOf(forTime);
		}
		Map<String, List<Game>> forTimeResults = new HashMap<String, List<Game>>();
		for (List<Game> game : this.getGames().values()) {
			List<Game> resultGames = this.getResultsForTimeAndGame(game, time, forTime);
			if (resultGames == null) {
				continue;
			}
			forTimeResults.put(resultGames.get(0).getId(), resultGames);

		}
		return forTimeResults;
	}

	public List<Game> getResultsForTimeAndGame(List<Game> game, Integer time, String forTime) {
		Game forGame = null;
		Game lastGame = game.size() > 0 ? game.get(game.size() - 1) : null;

		if (lastGame == null) {
			return null;
		}

		for (int i = game.size() - 1; i >= 0; i--) {
			int lastStatus = lastGame.getStatus().equals("half") ? 45
					: Integer.valueOf(game.get(game.size() - 1).getStatus());
			if (forTime.equals(game.get(i).getStatus()) || (!game.get(i).getStatus().contentEquals("half")
					&& time != null && Integer.valueOf(game.get(i).getStatus()) == lastStatus - time)) {
				forGame = game.get(i);
				break;
			}
		}
		List<Game> games = new ArrayList<Game>();
		if (forGame != null) {
			Game subGame = new Game();
			subGame.setId(forGame.getId());
			subGame.setH(forGame.getH());
			subGame.setA(forGame.getA());
			subGame.setA_id(forGame.getA_id());
			subGame.setH_id(forGame.getH_id());
			subGame.setPossess(lastGame.getPossess());
			subGame.setPossess_h(lastGame.getPossess_h());
			subGame.setAg(lastGame.getAg());
			subGame.setHg(lastGame.getHg());
			subGame.setStatus(lastGame.getStatus());
			subGame.setAc(this.subtractI(lastGame.getAc(), forGame.getAc()));
			subGame.setHc(this.subtractI(lastGame.getHc(), forGame.getHc()));
			subGame.setL(forGame.getL());
			subGame.setAttacks(this.subtractI(lastGame.getAttacks(), forGame.getAttacks()));
			subGame.setAttacks_h(lastGame.getAttacks_h());
			subGame.setDang_attacks(this.subtractI(lastGame.getDang_attacks(), forGame.getDang_attacks()));
			subGame.setDang_attacks_h(lastGame.getDang_attacks_h());
			subGame.setShot_on(this.subtractI(lastGame.getShot_on(), forGame.getShot_on()));
			subGame.setShot_off(this.subtractI(lastGame.getShot_off(), forGame.getShot_off()));
			subGame.setShot_off_h(lastGame.getShot_off_h());
			subGame.setShot_on_h(lastGame.getShot_on_h());
			GameStatisticWorker.calculatePunti(subGame);
			GameStatisticWorker.calculateIP(subGame, (Double.valueOf(this.getTime(lastGame.getStatus())) - 45));
			subGame.setIp(GameStatisticWorker.round(subGame.getIp()));
			subGame.setIpp(GameStatisticWorker.round(subGame.getIpp()));
			games.add(subGame);
			games.add(forGame);
			games.add(lastGame);
		} else {
			games.add(lastGame);
		}
		return games;
	}

	private String getTime(String time) {
		return time.equals("half") ? "45" : time;
	}

	private String[] subtractI(String[] first, String[] second) {
		String[] result = new String[2];
		result[0] = String.valueOf(Integer.valueOf(first[0]) - Integer.valueOf(second[0]));
		result[1] = String.valueOf(Integer.valueOf(first[1]) - Integer.valueOf(second[1]));
		return result;
	}

	private String subtractI(String first, String second) {
		return String.valueOf(Integer.valueOf(first) - Integer.valueOf(second));
	}

	public Map<String, List<Game>> getLastGameResult() {
		Map<String, List<Game>> games = new HashMap<String, List<Game>>();
		for (Entry<String, List<Game>> entry : this.games.entrySet()) {
			List<Game> g = new ArrayList<Game>();
			if (entry.getValue().size() > 0) {
				g.add(entry.getValue().get(entry.getValue().size() - 1));
				games.put(entry.getKey(), g);
			}
		}
		return games;
	}

	public void setGames(Map<String, List<Game>> games) {
		this.games = games;
	}

	public void addGameRecord(Game game) {
		if (this.games.containsKey(game.getId())) {
			this.games.get(game.getId()).add(game);
		} else {
			this.games.put(game.getId(), new ArrayList<Game>());
		}
	}

	public Map<String, TelegramNotification> getSentGames() {
		return sentGames;
	}

	public void setSentGames(Map<String, TelegramNotification> sentGames) {
		this.sentGames = sentGames;
	}

	public Map<String, TelegramNotification> getGamesToBeSend() {
		return gamesToBeSend;
	}

	public void setGamesToBeSend(Map<String, TelegramNotification> gamesToBeSend) {
		this.gamesToBeSend = gamesToBeSend;
	}

}
