package com.game.statistic.worker;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class TelegramComponent {

	private String url = "https://api.telegram.org/bot901941370:AAE6r9XCqxPSYkU9eKo7_4ROnEENjn3AREY/sendMessage?chat_id=-438774738&text=";

	// private String url =
	// "https://api.telegram.org/bot929959214:AAHYC9I6LX8fsNHAvqraG0w3ncjNM_VCyMU/sendMessage?chat_id=-359676530&text=";
	private RestTemplate restTemplate = new RestTemplate();

	public void sendTelegramMessage(String text) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("user-agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> response = restTemplate.exchange(this.url + text, HttpMethod.GET, entity, String.class);
		if (response.getStatusCode() == HttpStatus.OK) {
			System.out.println("Telegram sent");
		}
	}

}
