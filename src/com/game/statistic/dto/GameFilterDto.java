package com.game.statistic.dto;

import java.util.UUID;

public class GameFilterDto {
	private UUID id;
	private String name;
	private Boolean isActive;
	private GameFilterIntegerItem status = new GameFilterIntegerItem();
	private GameFilterIntegerItem hc = new GameFilterIntegerItem();
	private GameFilterIntegerItem ac = new GameFilterIntegerItem();
	private GameFilterIntegerItem hg = new GameFilterIntegerItem();
	private GameFilterIntegerItem ag = new GameFilterIntegerItem();
	private GameFilterIntegerItem hrc = new GameFilterIntegerItem();
	private GameFilterIntegerItem arc = new GameFilterIntegerItem();
	private GameFilterIntegerItem hyc = new GameFilterIntegerItem();
	private GameFilterIntegerItem ayc = new GameFilterIntegerItem();
	private GameFilterIntegerItem hAttacks = new GameFilterIntegerItem();
	private GameFilterIntegerItem aAttacks = new GameFilterIntegerItem();
	private GameFilterIntegerItem hShot_on = new GameFilterIntegerItem();
	private GameFilterIntegerItem aShot_on = new GameFilterIntegerItem();
	private GameFilterIntegerItem tShot_on = new GameFilterIntegerItem();
	private GameFilterIntegerItem hShot_off = new GameFilterIntegerItem();
	private GameFilterIntegerItem aShot_off = new GameFilterIntegerItem();
	private GameFilterIntegerItem tShot_off = new GameFilterIntegerItem();
	private GameFilterIntegerItem hPossess = new GameFilterIntegerItem();
	private GameFilterIntegerItem aPossess = new GameFilterIntegerItem();
	private GameFilterIntegerItem hDang_attacks = new GameFilterIntegerItem();
	private GameFilterIntegerItem aDang_attacks = new GameFilterIntegerItem();
	private GameFilterIntegerItem tGoal = new GameFilterIntegerItem();
	private GameFilterIntegerItem tAtt = new GameFilterIntegerItem();
	private GameFilterIntegerItem tDatt = new GameFilterIntegerItem();
	private GameFilterDoubleItem tPunti = new GameFilterDoubleItem();
	private GameFilterIntegerItem diffPoss = new GameFilterIntegerItem();
	// Calculate
	private GameFilterDoubleItem hPunti = new GameFilterDoubleItem();
	private GameFilterDoubleItem aPunti = new GameFilterDoubleItem();
	private GameFilterDoubleItem ip = new GameFilterDoubleItem();
	private GameFilterDoubleItem ipp = new GameFilterDoubleItem();

	public GameFilterIntegerItem getStatus() {
		return status;
	}

	public void setStatus(GameFilterIntegerItem status) {
		this.status = status;
	}

	public GameFilterIntegerItem getHc() {
		return hc;
	}

	public void setHc(GameFilterIntegerItem hc) {
		this.hc = hc;
	}

	public GameFilterIntegerItem getAc() {
		return ac;
	}

	public void setAc(GameFilterIntegerItem ac) {
		this.ac = ac;
	}

	public GameFilterIntegerItem getHg() {
		return hg;
	}

	public void setHg(GameFilterIntegerItem hg) {
		this.hg = hg;
	}

	public GameFilterIntegerItem getAg() {
		return ag;
	}

	public void setAg(GameFilterIntegerItem ag) {
		this.ag = ag;
	}

	public GameFilterIntegerItem getHrc() {
		return hrc;
	}

	public void setHrc(GameFilterIntegerItem hrc) {
		this.hrc = hrc;
	}

	public GameFilterIntegerItem getArc() {
		return arc;
	}

	public void setArc(GameFilterIntegerItem arc) {
		this.arc = arc;
	}

	public GameFilterIntegerItem getHyc() {
		return hyc;
	}

	public void setHyc(GameFilterIntegerItem hyc) {
		this.hyc = hyc;
	}

	public GameFilterIntegerItem getAyc() {
		return ayc;
	}

	public void setAyc(GameFilterIntegerItem ayc) {
		this.ayc = ayc;
	}

	public GameFilterIntegerItem gethAttacks() {
		return hAttacks;
	}

	public void sethAttacks(GameFilterIntegerItem hAttacks) {
		this.hAttacks = hAttacks;
	}

	public GameFilterIntegerItem getaAttacks() {
		return aAttacks;
	}

	public void setaAttacks(GameFilterIntegerItem aAttacks) {
		this.aAttacks = aAttacks;
	}

	public GameFilterIntegerItem gethShot_on() {
		return hShot_on;
	}

	public void sethShot_on(GameFilterIntegerItem hShot_on) {
		this.hShot_on = hShot_on;
	}

	public GameFilterIntegerItem getaShot_on() {
		return aShot_on;
	}

	public void setaShot_on(GameFilterIntegerItem aShot_on) {
		this.aShot_on = aShot_on;
	}

	public GameFilterIntegerItem gethShot_off() {
		return hShot_off;
	}

	public void sethShot_off(GameFilterIntegerItem hShot_off) {
		this.hShot_off = hShot_off;
	}

	public GameFilterIntegerItem getaShot_off() {
		return aShot_off;
	}

	public void setaShot_off(GameFilterIntegerItem aShot_off) {
		this.aShot_off = aShot_off;
	}

	public GameFilterIntegerItem gethPossess() {
		return hPossess;
	}

	public void sethPossess(GameFilterIntegerItem hPossess) {
		this.hPossess = hPossess;
	}

	public GameFilterIntegerItem getaPossess() {
		return aPossess;
	}

	public void setaPossess(GameFilterIntegerItem aPossess) {
		this.aPossess = aPossess;
	}

	public GameFilterIntegerItem gethDang_attacks() {
		return hDang_attacks;
	}

	public void sethDang_attacks(GameFilterIntegerItem hDang_attacks) {
		this.hDang_attacks = hDang_attacks;
	}

	public GameFilterIntegerItem getaDang_attacks() {
		return aDang_attacks;
	}

	public void setaDang_attacks(GameFilterIntegerItem aDang_attacks) {
		this.aDang_attacks = aDang_attacks;
	}

	public GameFilterDoubleItem gethPunti() {
		return hPunti;
	}

	public void sethPunti(GameFilterDoubleItem hPunti) {
		this.hPunti = hPunti;
	}

	public GameFilterDoubleItem getaPunti() {
		return aPunti;
	}

	public void setaPunti(GameFilterDoubleItem aPunti) {
		this.aPunti = aPunti;
	}

	public GameFilterDoubleItem getIp() {
		return ip;
	}

	public void setIp(GameFilterDoubleItem ip) {
		this.ip = ip;
	}

	public GameFilterDoubleItem getIpp() {
		return ipp;
	}

	public void setIpp(GameFilterDoubleItem ipp) {
		this.ipp = ipp;
	}

	public GameFilterIntegerItem gettShot_on() {
		return tShot_on;
	}

	public void settShot_on(GameFilterIntegerItem tShot_on) {
		this.tShot_on = tShot_on;
	}

	public GameFilterIntegerItem gettShot_off() {
		return tShot_off;
	}

	public void settShot_off(GameFilterIntegerItem tShot_off) {
		this.tShot_off = tShot_off;
	}

	public UUID getId() {
		return id;
	}

	public String getIdString() {
		return id.toString();
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public GameFilterIntegerItem gettGoal() {
		return tGoal;
	}

	public void settGoal(GameFilterIntegerItem tGoal) {
		this.tGoal = tGoal;
	}

	public GameFilterIntegerItem gettAtt() {
		return tAtt;
	}

	public void settAtt(GameFilterIntegerItem tAtt) {
		this.tAtt = tAtt;
	}

	public GameFilterIntegerItem gettDatt() {
		return tDatt;
	}

	public void settDatt(GameFilterIntegerItem tDatt) {
		this.tDatt = tDatt;
	}

	public GameFilterDoubleItem gettPunti() {
		return tPunti;
	}

	public void settPunti(GameFilterDoubleItem tPunti) {
		this.tPunti = tPunti;
	}

	public GameFilterIntegerItem getDiffPoss() {
		return diffPoss;
	}

	public void setDiffPoss(GameFilterIntegerItem diffPoss) {
		this.diffPoss = diffPoss;
	}

}
