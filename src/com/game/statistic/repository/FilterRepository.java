package com.game.statistic.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.game.statistic.model.GameFilter;

@Repository
public interface FilterRepository extends JpaRepository<GameFilter, UUID> {

}
