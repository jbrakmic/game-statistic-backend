package com.game.statistic.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "GAME_FILTER")
public class GameFilter {
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "GAME_FILTER_ID", columnDefinition = "BINARY(16)")
	private UUID id;
	@Column
	private String name;
	@Column
	private Boolean isActive;
	@Column
	private Integer status_min;
	@Column
	private Integer status_max;
	@Column
	private Integer hc_min;
	@Column
	private Integer hc_max;
	@Column
	private Integer ac_min;
	@Column
	private Integer ac_max;
	@Column
	private Integer hg_min;
	@Column
	private Integer hg_max;
	@Column
	private Integer ag_min;
	@Column
	private Integer ag_max;
	@Column
	private Integer hrc_min;
	@Column
	private Integer hrc_max;
	@Column
	private Integer arc_min;
	@Column
	private Integer arc_max;
	@Column
	private Integer hyc_min;
	@Column
	private Integer hyc_max;
	@Column
	private Integer ayc_min;
	@Column
	private Integer ayc_max;
	@Column
	private Integer hAttacks_min;
	@Column
	private Integer hAttacks_max;
	@Column
	private Integer aAttacks_min;
	@Column
	private Integer aAttacks_max;
	@Column
	private Integer hShot_on_min;
	@Column
	private Integer hShot_on_max;
	@Column
	private Integer aShot_on_min;
	@Column
	private Integer aShot_on_max;
	@Column
	private Integer tShot_on_min;
	@Column
	private Integer tShot_on_max;
	@Column
	private Integer hShot_off_min;
	@Column
	private Integer hShot_off_max;
	@Column
	private Integer aShot_off_min;
	@Column
	private Integer aShot_off_max;
	@Column
	private Integer tShot_off_min;
	@Column
	private Integer tShot_off_max;
	@Column
	private Integer hPossess_min;
	@Column
	private Integer hPossess_max;
	@Column
	private Integer aPossess_min;
	@Column
	private Integer aPossess_max;
	@Column
	private Integer diffPoss_min;
	@Column
	private Integer diffPoss_max;
	@Column
	private Integer hDang_attacks_min;
	@Column
	private Integer hDang_attacks_max;
	@Column
	private Integer aDang_attacks_min;
	@Column
	private Integer aDang_attacks_max;
	@Column
	private Integer tGoal_min;
	@Column
	private Integer tGoal_max;
	@Column
	private Integer tDang_min;
	@Column
	private Integer tDang_max;
	@Column
	private Integer tAtt_min;
	@Column
	private Integer tAtt_max;
	@Column
	private Double tPunti_min;
	@Column
	private Double tPunti_max;
	// Calculate
	@Column
	private Double hPunti_min;
	@Column
	private Double hPunti_max;
	@Column
	private Double aPunti_min;
	@Column
	private Double aPunti_max;
	@Column
	private Double ip_min;
	@Column
	private Double ip_max;
	@Column
	private Double ipp_min;
	@Column
	private Double ipp_max;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getStatus_min() {
		return status_min;
	}

	public void setStatus_min(Integer status_min) {
		this.status_min = status_min;
	}

	public Integer getStatus_max() {
		return status_max;
	}

	public void setStatus_max(Integer status_max) {
		this.status_max = status_max;
	}

	public Integer getHc_min() {
		return hc_min;
	}

	public void setHc_min(Integer hc_min) {
		this.hc_min = hc_min;
	}

	public Integer getHc_max() {
		return hc_max;
	}

	public void setHc_max(Integer hc_max) {
		this.hc_max = hc_max;
	}

	public Integer getAc_min() {
		return ac_min;
	}

	public void setAc_min(Integer ac_min) {
		this.ac_min = ac_min;
	}

	public Integer getAc_max() {
		return ac_max;
	}

	public void setAc_max(Integer ac_max) {
		this.ac_max = ac_max;
	}

	public Integer getHg_min() {
		return hg_min;
	}

	public void setHg_min(Integer hg_min) {
		this.hg_min = hg_min;
	}

	public Integer getHg_max() {
		return hg_max;
	}

	public void setHg_max(Integer hg_max) {
		this.hg_max = hg_max;
	}

	public Integer getAg_min() {
		return ag_min;
	}

	public void setAg_min(Integer ag_min) {
		this.ag_min = ag_min;
	}

	public Double getaPunti_max() {
		return aPunti_max;
	}

	public void setaPunti_max(Double aPunti_max) {
		this.aPunti_max = aPunti_max;
	}

	public Integer getAg_max() {
		return ag_max;
	}

	public void setAg_max(Integer ag_max) {
		this.ag_max = ag_max;
	}

	public Integer getHrc_min() {
		return hrc_min;
	}

	public void setHrc_min(Integer hrc_min) {
		this.hrc_min = hrc_min;
	}

	public Integer getHrc_max() {
		return hrc_max;
	}

	public void setHrc_max(Integer hrc_max) {
		this.hrc_max = hrc_max;
	}

	public Integer getArc_min() {
		return arc_min;
	}

	public void setArc_min(Integer arc_min) {
		this.arc_min = arc_min;
	}

	public Integer getArc_max() {
		return arc_max;
	}

	public void setArc_max(Integer arc_max) {
		this.arc_max = arc_max;
	}

	public Integer getHyc_min() {
		return hyc_min;
	}

	public void setHyc_min(Integer hyc_min) {
		this.hyc_min = hyc_min;
	}

	public Integer getHyc_max() {
		return hyc_max;
	}

	public void setHyc_max(Integer hyc_max) {
		this.hyc_max = hyc_max;
	}

	public Integer getAyc_min() {
		return ayc_min;
	}

	public void setAyc_min(Integer ayc_min) {
		this.ayc_min = ayc_min;
	}

	public Integer getAyc_max() {
		return ayc_max;
	}

	public void setAyc_max(Integer ayc_max) {
		this.ayc_max = ayc_max;
	}

	public Integer gethAttacks_min() {
		return hAttacks_min;
	}

	public void sethAttacks_min(Integer hAttacks_min) {
		this.hAttacks_min = hAttacks_min;
	}

	public Integer gethAttacks_max() {
		return hAttacks_max;
	}

	public void sethAttacks_max(Integer hAttacks_max) {
		this.hAttacks_max = hAttacks_max;
	}

	public Integer getaAttacks_min() {
		return aAttacks_min;
	}

	public void setaAttacks_min(Integer aAttacks_min) {
		this.aAttacks_min = aAttacks_min;
	}

	public Integer getaAttacks_max() {
		return aAttacks_max;
	}

	public void setaAttacks_max(Integer aAttacks_max) {
		this.aAttacks_max = aAttacks_max;
	}

	public Integer gethShot_on_min() {
		return hShot_on_min;
	}

	public void sethShot_on_min(Integer hShot_on_min) {
		this.hShot_on_min = hShot_on_min;
	}

	public Integer gethShot_on_max() {
		return hShot_on_max;
	}

	public void sethShot_on_max(Integer hShot_on_max) {
		this.hShot_on_max = hShot_on_max;
	}

	public Integer getaShot_on_min() {
		return aShot_on_min;
	}

	public void setaShot_on_min(Integer aShot_on_min) {
		this.aShot_on_min = aShot_on_min;
	}

	public Integer getaShot_on_max() {
		return aShot_on_max;
	}

	public void setaShot_on_max(Integer aShot_on_max) {
		this.aShot_on_max = aShot_on_max;
	}

	public Integer gettShot_on_min() {
		return tShot_on_min;
	}

	public void settShot_on_min(Integer tShot_on_min) {
		this.tShot_on_min = tShot_on_min;
	}

	public Integer gettShot_on_max() {
		return tShot_on_max;
	}

	public void settShot_on_max(Integer tShot_on_max) {
		this.tShot_on_max = tShot_on_max;
	}

	public Integer gethShot_off_min() {
		return hShot_off_min;
	}

	public void sethShot_off_min(Integer hShot_off_min) {
		this.hShot_off_min = hShot_off_min;
	}

	public Integer gethShot_off_max() {
		return hShot_off_max;
	}

	public void sethShot_off_max(Integer hShot_off_max) {
		this.hShot_off_max = hShot_off_max;
	}

	public Integer getaShot_off_min() {
		return aShot_off_min;
	}

	public void setaShot_off_min(Integer aShot_off_min) {
		this.aShot_off_min = aShot_off_min;
	}

	public Integer getaShot_off_max() {
		return aShot_off_max;
	}

	public void setaShot_off_max(Integer aShot_off_max) {
		this.aShot_off_max = aShot_off_max;
	}

	public Integer gettShot_off_min() {
		return tShot_off_min;
	}

	public void settShot_off_min(Integer tShot_off_min) {
		this.tShot_off_min = tShot_off_min;
	}

	public Integer gettShot_off_max() {
		return tShot_off_max;
	}

	public void settShot_off_max(Integer tShot_off_max) {
		this.tShot_off_max = tShot_off_max;
	}

	public Integer gethPossess_min() {
		return hPossess_min;
	}

	public void sethPossess_min(Integer hPossess_min) {
		this.hPossess_min = hPossess_min;
	}

	public Integer gethPossess_max() {
		return hPossess_max;
	}

	public void sethPossess_max(Integer hPossess_max) {
		this.hPossess_max = hPossess_max;
	}

	public Integer getaPossess_min() {
		return aPossess_min;
	}

	public void setaPossess_min(Integer aPossess_min) {
		this.aPossess_min = aPossess_min;
	}

	public Integer getaPossess_max() {
		return aPossess_max;
	}

	public void setaPossess_max(Integer aPossess_max) {
		this.aPossess_max = aPossess_max;
	}

	public Integer gethDang_attacks_min() {
		return hDang_attacks_min;
	}

	public void sethDang_attacks_min(Integer hDang_attacks_min) {
		this.hDang_attacks_min = hDang_attacks_min;
	}

	public Integer gethDang_attacks_max() {
		return hDang_attacks_max;
	}

	public void sethDang_attacks_max(Integer hDang_attacks_max) {
		this.hDang_attacks_max = hDang_attacks_max;
	}

	public Integer getaDang_attacks_min() {
		return aDang_attacks_min;
	}

	public void setaDang_attacks_min(Integer aDang_attacks_min) {
		this.aDang_attacks_min = aDang_attacks_min;
	}

	public Integer getaDang_attacks_max() {
		return aDang_attacks_max;
	}

	public void setaDang_attacks_max(Integer aDang_attacks_max) {
		this.aDang_attacks_max = aDang_attacks_max;
	}

	public Double gethPunti_min() {
		return hPunti_min;
	}

	public void sethPunti_min(Double hPunti_min) {
		this.hPunti_min = hPunti_min;
	}

	public Double gethPunti_max() {
		return hPunti_max;
	}

	public void sethPunti_max(Double hPunti_max) {
		this.hPunti_max = hPunti_max;
	}

	public Double getaPunti_min() {
		return aPunti_min;
	}

	public void setaPunti_min(Double aPunti_min) {
		this.aPunti_min = aPunti_min;
	}

	public Double getIp_min() {
		return ip_min;
	}

	public void setIp_min(Double ip_min) {
		this.ip_min = ip_min;
	}

	public Double getIp_max() {
		return ip_max;
	}

	public void setIp_max(Double ip_max) {
		this.ip_max = ip_max;
	}

	public Double getIpp_min() {
		return ipp_min;
	}

	public void setIpp_min(Double ipp_min) {
		this.ipp_min = ipp_min;
	}

	public Double getIpp_max() {
		return ipp_max;
	}

	public void setIpp_max(Double ipp_max) {
		this.ipp_max = ipp_max;
	}

	public Integer gettGoal_min() {
		return tGoal_min;
	}

	public void settGoal_min(Integer tGoal_min) {
		this.tGoal_min = tGoal_min;
	}

	public Integer gettGoal_max() {
		return tGoal_max;
	}

	public void settGoal_max(Integer tGoal_max) {
		this.tGoal_max = tGoal_max;
	}

	public Integer gettDang_min() {
		return tDang_min;
	}

	public void settDang_min(Integer tDang_min) {
		this.tDang_min = tDang_min;
	}

	public Integer gettDang_max() {
		return tDang_max;
	}

	public void settDang_max(Integer tDang_max) {
		this.tDang_max = tDang_max;
	}

	public Integer gettAtt_min() {
		return tAtt_min;
	}

	public void settAtt_min(Integer tAtt_min) {
		this.tAtt_min = tAtt_min;
	}

	public Integer gettAtt_max() {
		return tAtt_max;
	}

	public void settAtt_max(Integer tAtt_max) {
		this.tAtt_max = tAtt_max;
	}

	public Double gettPunti_min() {
		return tPunti_min;
	}

	public void settPunti_min(Double tPunti_min) {
		this.tPunti_min = tPunti_min;
	}

	public Double gettPunti_max() {
		return tPunti_max;
	}

	public void settPunti_max(Double tPunti_max) {
		this.tPunti_max = tPunti_max;
	}

	public Integer getDiffPoss_min() {
		return diffPoss_min;
	}

	public void setDiffPoss_min(Integer diffPoss_min) {
		this.diffPoss_min = diffPoss_min;
	}

	public Integer getDiffPoss_max() {
		return diffPoss_max;
	}

	public void setDiffPoss_max(Integer diffPoss_max) {
		this.diffPoss_max = diffPoss_max;
	}

}
