package com.game.statistic.dto;

import java.util.List;

import com.game.statistic.model.Game;

public class GameStatisticResponseDto {

	private Integer success;
	private List<Game> data;
	public Integer getSuccess() {
		return success;
	}
	public void setSuccess(Integer success) {
		this.success = success;
	}
	public List<Game> getData() {
		return data;
	}
	public void setData(List<Game> data) {
		this.data = data;
	}

}
