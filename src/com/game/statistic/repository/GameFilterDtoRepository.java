package com.game.statistic.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.statistic.dto.GameFilterDto;
import com.game.statistic.factory.GameFilterFactory;
import com.game.statistic.model.GameFilter;

@Repository
public class GameFilterDtoRepository {

	@Autowired
	private FilterRepository filterRepository;

	private Map<UUID, GameFilterDto> gameFilters = new HashMap<UUID, GameFilterDto>();

	@PostConstruct
	public void init() {
		List<GameFilter> filters = this.filterRepository.findAll();
		for (GameFilter gameFilter : filters) {
			this.gameFilters.put(gameFilter.getId(), GameFilterFactory.createGameFilterDto(gameFilter));
		}
	}

	public Map<UUID, GameFilterDto> getGameFilters() {
		return gameFilters;
	}

	public void setGameFilters(Map<UUID, GameFilterDto> gameFilters) {
		this.gameFilters = gameFilters;
	}

	public void removeGameFilter(String id) {
		this.gameFilters.remove(UUID.fromString(id));
	}

}
