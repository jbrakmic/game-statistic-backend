package com.game.statistic.model;

import java.util.Date;

import com.game.statistic.dto.GameFilterDto;

public class TelegramNotification {
	private String id;
	private Date timestamp;
	private GameFilterDto filter;
	private Game game;

	public TelegramNotification(String id, GameFilterDto filter, Game game) {
		super();
		this.filter = filter;
		this.game = game;
		this.timestamp = new Date();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public GameFilterDto getFilter() {
		return filter;
	}

	public void setFilter(GameFilterDto filter) {
		this.filter = filter;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

}
