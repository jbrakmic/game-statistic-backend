package com.game.statistic.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.game.statistic.dto.GameFilterDto;
import com.game.statistic.dto.ToggleActivationRequest;
import com.game.statistic.factory.GameFilterFactory;
import com.game.statistic.model.Game;
import com.game.statistic.model.GameFilter;
import com.game.statistic.repository.FilterRepository;
import com.game.statistic.repository.GameFilterDtoRepository;
import com.game.statistic.repository.GameRepository;
import com.game.statistic.repository.IGameRepository;

@RestController
@RequestMapping("game")
public class GameController {

	@Autowired
	private GameRepository gameRepository;
	@Autowired
	private FilterRepository gameFilterRepository;
	@Autowired
	private GameFilterDtoRepository gameFilterDtoRepository;
	@Autowired
	private IGameRepository dbGameRepository;

	@PostMapping(value = "/filter", consumes = "application/json", produces = "application/json")
	public void saveGameFilter(@RequestBody GameFilterDto gameFilter) {
		GameFilter filter = this.gameFilterRepository.save(GameFilterFactory.createGameFilter(gameFilter));
		gameFilter.setId(filter.getId());
		this.gameFilterDtoRepository.getGameFilters().put(filter.getId(), gameFilter);
	}

	@GetMapping(value = "/archive", produces = "application/json")
	public List<Game> getArchive() {
		List<Game> games = this.dbGameRepository.findAll();
		return games;
	}

	@GetMapping(value = "/filter", consumes = "application/json", produces = "application/json")
	public List<GameFilterDto> getGameFilter() {
		List<GameFilterDto> dtos = new ArrayList<GameFilterDto>();
		List<GameFilter> gameFilters = this.gameFilterRepository.findAll();
		for (GameFilter f : gameFilters) {
			dtos.add(GameFilterFactory.createGameFilterDto(f));
		}
		return dtos;
	}

	@PutMapping(value = "/filter", consumes = "application/json", produces = "application/json")
	public void deleteFilters(@RequestBody List<String> filterIds) {
		if (filterIds == null || filterIds.isEmpty()) {
			return;
		}
		for (String id : filterIds) {
			this.gameFilterRepository.deleteById(UUID.fromString(id));
			this.gameFilterDtoRepository.removeGameFilter(id);
			this.gameRepository.getSentGames().entrySet()
					.removeIf(e -> e.getValue().getFilter().getId().toString().equals(id));
			this.gameRepository.getGamesToBeSend().entrySet()
					.removeIf(e -> e.getValue().getFilter().getId().toString().equals(id));
		}

	}

	@PutMapping(value = "/filter/activation", consumes = "application/json", produces = "application/json")
	public void toggleActivation(@RequestBody List<ToggleActivationRequest> request) {
		if (request == null || request.isEmpty()) {
			return;
		}
		for (ToggleActivationRequest toggle : request) {
			GameFilter filter = this.gameFilterRepository.getOne(UUID.fromString(toggle.getFilterId()));
			filter.setIsActive(toggle.getActive());
			this.gameFilterRepository.save(filter);
			GameFilterDto filterDto = this.gameFilterDtoRepository.getGameFilters()
					.get(UUID.fromString(toggle.getFilterId()));
			filterDto.setIsActive(toggle.getActive());
		}
	}

	@GetMapping(value = "/statistic", consumes = "application/json", produces = "application/json")
	public Map<String, List<Game>> getGameStatistic(@RequestParam(value = "forTime", required = false) String forTime) {
		if (forTime == null || forTime.isEmpty()) {
			return this.gameRepository.getLastGameResult();
		}
		return this.gameRepository.getResultsForTime(forTime);
	}

}
