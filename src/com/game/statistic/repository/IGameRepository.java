package com.game.statistic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.game.statistic.model.Game;

@Repository
public interface IGameRepository extends JpaRepository<Game, String> {

}
