package com.game.statistic.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "GAME")
public class Game {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "ID", columnDefinition = "BINARY(16)")
	private UUID gid;

	@Column(name = "GAME_ID")
	private String id;
	@Column
	private String h;
	private String h_id;
	@Column
	private String a;
	private String a_id;
	@Column
	private String l;
	private String l_id;
	private String start;
	@Column
	private String status;
	@Column
	private String hc;
	@Column
	private String ac;
	@Column
	private String hg;
	@Column
	private String ag;
	@Column
	private String hrc;
	@Column
	private String arc;
	@Column
	private String hyc;
	@Column
	private String ayc;
	@Column
	private String storingType;
	private String hf_hc;
	private String hf_ac;
	private String hf_hg;
	private String hf_ag;
	private String ish;
	private Date timestamp;
	private String[] p_odds;
	private String[] po_odds;
	private String[] i_odds;
	private String[] p_asian;
	private String[] i_asian;
	private String[] p_corner;

	public UUID getGid() {
		return gid;
	}

	public void setGid(UUID gid) {
		this.gid = gid;
	}

	private String[] i_corner;
	private String[] p_goal;
	private String[] i_goal;
	private String[] asian_corner;
	private String[] attacks;
	private String[] attacks_h;
	private String[] shot_on;
	private String[] shot_on_h;
	private String[] shot_off;
	private String[] shot_off_h;
	private String[] possess;
	private String[] possess_h;
	private String[] dang_attacks;
	private String[] dang_attacks_h;

	private Integer timeFromLastGoal = null;

	// Calculate
	@Column
	private Double hPunti;
	@Column
	private Double aPunti;
	@Column
	private Double ip;
	private Double ip1H;
	private Double ip1A;
	@Column
	private Double ipp;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getH() {
		return h;
	}

	public void setH(String h) {
		this.h = h;
	}

	public String getH_id() {
		return h_id;
	}

	public void setH_id(String h_id) {
		this.h_id = h_id;
	}

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	public String getA_id() {
		return a_id;
	}

	public void setA_id(String a_id) {
		this.a_id = a_id;
	}

	public String getL() {
		return l;
	}

	public void setL(String l) {
		this.l = l;
	}

	public String getL_id() {
		return l_id;
	}

	public void setL_id(String l_id) {
		this.l_id = l_id;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHc() {
		return hc;
	}

	public void setHc(String hc) {
		this.hc = hc;
	}

	public String getAc() {
		return ac;
	}

	public void setAc(String ac) {
		this.ac = ac;
	}

	public String getHg() {
		return hg;
	}

	public void setHg(String hg) {
		this.hg = hg;
	}

	public String getAg() {
		return ag;
	}

	public void setAg(String ag) {
		this.ag = ag;
	}

	public String getHrc() {
		return hrc;
	}

	public void setHrc(String hrc) {
		this.hrc = hrc;
	}

	public String getArc() {
		return arc;
	}

	public void setArc(String arc) {
		this.arc = arc;
	}

	public String getHyc() {
		return hyc;
	}

	public void setHyc(String hyc) {
		this.hyc = hyc;
	}

	public String getAyc() {
		return ayc;
	}

	public void setAyc(String ayc) {
		this.ayc = ayc;
	}

	public String getHf_hc() {
		return hf_hc;
	}

	public void setHf_hc(String hf_hc) {
		this.hf_hc = hf_hc;
	}

	public String getHf_ac() {
		return hf_ac;
	}

	public void setHf_ac(String hf_ac) {
		this.hf_ac = hf_ac;
	}

	public String getHf_hg() {
		return hf_hg;
	}

	public void setHf_hg(String hf_hg) {
		this.hf_hg = hf_hg;
	}

	public String getHf_ag() {
		return hf_ag;
	}

	public void setHf_ag(String hf_ag) {
		this.hf_ag = hf_ag;
	}

	public String getIsh() {
		return ish;
	}

	public String[] getP_odds() {
		return p_odds;
	}

	public void setP_odds(String[] p_odds) {
		this.p_odds = p_odds;
	}

	public String[] getPo_odds() {
		return po_odds;
	}

	public void setPo_odds(String[] po_odds) {
		this.po_odds = po_odds;
	}

	public String[] getI_odds() {
		return i_odds;
	}

	public void setI_odds(String[] i_odds) {
		this.i_odds = i_odds;
	}

	public String[] getP_asian() {
		return p_asian;
	}

	public void setP_asian(String[] p_asian) {
		this.p_asian = p_asian;
	}

	public String[] getI_asian() {
		return i_asian;
	}

	public void setI_asian(String[] i_asian) {
		this.i_asian = i_asian;
	}

	public String[] getP_corner() {
		return p_corner;
	}

	public void setP_corner(String[] p_corner) {
		this.p_corner = p_corner;
	}

	public String[] getI_corner() {
		return i_corner;
	}

	public void setI_corner(String[] i_corner) {
		this.i_corner = i_corner;
	}

	public String[] getP_goal() {
		return p_goal;
	}

	public void setP_goal(String[] p_goal) {
		this.p_goal = p_goal;
	}

	public String[] getI_goal() {
		return i_goal;
	}

	public void setI_goal(String[] i_goal) {
		this.i_goal = i_goal;
	}

	public String[] getAsian_corner() {
		return asian_corner;
	}

	public void setAsian_corner(String[] asian_corner) {
		this.asian_corner = asian_corner;
	}

	public String[] getAttacks() {
		return attacks;
	}

	public void setAttacks(String[] attacks) {
		this.attacks = attacks;
	}

	public String[] getAttacks_h() {
		return attacks_h;
	}

	public void setAttacks_h(String[] attacks_h) {
		this.attacks_h = attacks_h;
	}

	public String[] getShot_on() {
		return shot_on;
	}

	public void setShot_on(String[] shot_on) {
		this.shot_on = shot_on;
	}

	public String[] getShot_on_h() {
		return shot_on_h;
	}

	public void setShot_on_h(String[] shot_on_h) {
		this.shot_on_h = shot_on_h;
	}

	public String[] getShot_off() {
		return shot_off;
	}

	public void setShot_off(String[] shot_off) {
		this.shot_off = shot_off;
	}

	public String[] getShot_off_h() {
		return shot_off_h;
	}

	public void setShot_off_h(String[] shot_off_h) {
		this.shot_off_h = shot_off_h;
	}

	public String[] getPossess() {
		return possess;
	}

	public void setPossess(String[] possess) {
		this.possess = possess;
	}

	public String[] getPossess_h() {
		return possess_h;
	}

	public void setPossess_h(String[] possess_h) {
		this.possess_h = possess_h;
	}

	public void setIsh(String ish) {
		this.ish = ish;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String[] getDang_attacks() {
		return dang_attacks;
	}

	public void setDang_attacks(String[] dang_attacks) {
		this.dang_attacks = dang_attacks;
	}

	public String[] getDang_attacks_h() {
		return dang_attacks_h;
	}

	public void setDang_attacks_h(String[] dang_attacks_h) {
		this.dang_attacks_h = dang_attacks_h;
	}

	public Double gethPunti() {
		return hPunti;
	}

	public void sethPunti(Double hPunti) {
		this.hPunti = hPunti;
	}

	public Double getaPunti() {
		return aPunti;
	}

	public void setaPunti(Double aPunti) {
		this.aPunti = aPunti;
	}

	public Double getIp() {
		return ip;
	}

	public void setIp(Double ip) {
		this.ip = ip;
	}

	public Double getIpp() {
		return ipp;
	}

	public void setIpp(Double ipp) {
		this.ipp = ipp;
	}

	public Double getIp1H() {
		return ip1H;
	}

	public void setIp1H(Double ip1h) {
		ip1H = ip1h;
	}

	public Double getIp1A() {
		return ip1A;
	}

	public void setIp1A(Double ip1a) {
		ip1A = ip1a;
	}

	public Integer getTimeFromLastGoal() {
		return timeFromLastGoal;
	}

	public void setTimeFromLastGoal(Integer timeFromLastGoal) {
		this.timeFromLastGoal = timeFromLastGoal;
	}

	public String getStoringType() {
		return storingType;
	}

	public void setStoringType(String storingType) {
		this.storingType = storingType;
	}

}
