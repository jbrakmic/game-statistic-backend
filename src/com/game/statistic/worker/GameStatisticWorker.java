package com.game.statistic.worker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.game.statistic.dto.GameFilterDoubleItem;
import com.game.statistic.dto.GameFilterDto;
import com.game.statistic.dto.GameFilterIntegerItem;
import com.game.statistic.dto.GameStatisticResponseDto;
import com.game.statistic.model.Game;
import com.game.statistic.model.TelegramNotification;
import com.game.statistic.repository.GameFilterDtoRepository;
import com.game.statistic.repository.GameRepository;
import com.game.statistic.repository.IGameRepository;

@Component
public class GameStatisticWorker {

	private RestTemplate restTemplate = new RestTemplate();
	private String gameStatUrl = "https://api.totalcorner.com/v1/match/today?token=dc078f095bf0b08d&type=inplay&fbclid=IwAR0Koo6OUGwRBoPvDNvYk17fi_ymEd4koUvl6Yr-iHCyPUb5OtYHQ9Qz-Dk&columns=odds,asian,cornerLine,goalLine,asianCorner,attacks,shotOn,shotOff,possession,dangerousAttacks";

	private Logger logger = LoggerFactory.getLogger(GameStatisticWorker.class);

	@Autowired
	private GameRepository gameRepository;
	@Autowired
	private TelegramComponent telegramComponent;
	@Autowired
	private GameFilterDtoRepository gameFilterRepository;
	@Autowired
	private IGameRepository dbGameRepository;

	@Scheduled(initialDelay = 10000, fixedRate = 60 * 1000)
	public void removeFinishedGames() {
		List<String> removedGames = new ArrayList<String>();
		for (List<Game> gameRecords : this.gameRepository.getGames().values()) {
			if (gameRecords.size() > 0
					&& (gameRecords.get(gameRecords.size() - 1).getTimestamp().getTime() + (15 * 60 * 1000)) < System
							.currentTimeMillis()) {
				removedGames.add(gameRecords.get(gameRecords.size() - 1).getId());
			}
		}

		for (String removedGame : removedGames) {
			this.gameRepository.getGames().remove(removedGame);
			this.gameRepository.getSentGames().entrySet()
					.removeIf(e -> e.getValue().getGame().getId().equals(removedGame));
		}
		this.logger.info("Removing old games " + removedGames.size());
	}

	@Scheduled(initialDelay = 5000, fixedRate = 5 * 1000)
	public synchronized void checkGamesForNotification() {
		Map<String, List<Game>> lastResults = this.gameRepository.getGames();
		Map<String, List<Game>> halfGameRecord = this.gameRepository.getResultsForTime("half");
		for (GameFilterDto dto : this.gameFilterRepository.getGameFilters().values()) {
			if (dto.getIsActive()) {
				for (List<Game> gameRecords : lastResults.values()) {
					if (gameRecords != null && !gameRecords.isEmpty()) {
						Game gameRecord = halfGameRecord.get(gameRecords.get(0).getId()).get(0);
						String key = this.getKey(dto.getIdString(), gameRecord.getId());
						if (this.shouldSendTelegram(gameRecord, dto)) {
							if (!this.isGoalFallenLast5Minutes(gameRecord, gameRecords)
									&& !this.gameRepository.getGamesToBeSend().containsKey(key)
									&& !this.gameRepository.getSentGames().containsKey(key)) {
								this.gameRepository.getGamesToBeSend().put(key,
										new TelegramNotification(key, dto, gameRecord));
								this.logger.info("Adding gamefilter item to list to be sent with id" + key);
							} else if (this.isGoalFallenLast5Minutes(gameRecord, gameRecords)
									&& this.gameRepository.getGamesToBeSend().containsKey(key)) {
								this.logger.info("Removing gamefilter item from list (goal) with id" + key);
								this.gameRepository.getGamesToBeSend().remove(key);
							}
						} else if (this.gameRepository.getGamesToBeSend().containsKey(key)) {
							this.logger.info("Removing gamefilter item from list with id" + key);
							this.gameRepository.getGamesToBeSend().remove(key);
						}
					}
				}
			}
		}
	}

	private String getKey(String filterKey, String gameKey) {
		return filterKey + gameKey;
	}

	@Scheduled(initialDelay = 7000, fixedRate = 10 * 1000)
	public synchronized void checkForSendTelegram() {
		List<String> keysToRemove = new ArrayList<String>();
		for (TelegramNotification notification : this.gameRepository.getGamesToBeSend().values()) {
			if ((new Date().getTime() - notification.getTimestamp().getTime()) / 1000 > 40) {
				this.logger.info("Sending gamefilter item to list to be sent with id" + notification.getId());
				notification.getGame().setStoringType(notification.getFilter().getName());
				this.dbGameRepository.save(notification.getGame());
				this.telegramComponent
						.sendTelegramMessage(this.createMessage(notification.getGame(), notification.getFilter()));
				this.gameRepository.getSentGames().put(notification.getId(), notification);
				keysToRemove.add(notification.getId());
			}
		}
		for (String s : keysToRemove) {
			this.gameRepository.getGamesToBeSend().remove(s);
		}
	}

	private boolean isGoalFallenLast5Minutes(Game gameRecord, List<Game> records) {
		/*
		 * for (int i = records.size() - 1; i >= 0; i--) { int recordStatus = Integer
		 * .valueOf(records.get(i).getStatus().equals("half") ? "45" :
		 * records.get(i).getStatus()); if
		 * ((!gameRecord.getHg().equals(records.get(i).getHg()) ||
		 * !gameRecord.getAg().equals(records.get(i).getAg())) &&
		 * (Integer.valueOf(gameRecord.getStatus().equals("half") ? "45" :
		 * gameRecord.getStatus()) - recordStatus) <= 5) { return true; } if
		 * (Integer.valueOf(gameRecord.getStatus().equals("half") ? "45" :
		 * gameRecord.getStatus()) - recordStatus > 5) { return false; } }
		 */
		return false;
	}

	private boolean shouldSendTelegram(Game gameRecord, GameFilterDto filter) {
		return this.checkFilterAttribute(gameRecord.getStatus(), filter.getStatus())
				&& this.checkFilterAttribute(gameRecord.getHg(), filter.getHg())
				&& this.checkFilterAttribute(gameRecord.getAg(), filter.getAg())
				&& this.checkFilterAttribute(gameRecord.getHc(), filter.getHc())
				&& this.checkFilterAttribute(gameRecord.getAc(), filter.getAc())
				&& this.checkFilterAttribute(gameRecord.getAttacks()[0], filter.gethAttacks())
				&& this.checkFilterAttribute(gameRecord.getAttacks()[1], filter.getaAttacks())
				&& this.checkFilterAttribute(gameRecord.getDang_attacks()[0], filter.gethDang_attacks())
				&& this.checkFilterAttribute(gameRecord.getDang_attacks()[1], filter.getaDang_attacks())
				&& this.checkFilterAttribute(gameRecord.getHrc(), filter.getHrc())
				&& this.checkFilterAttribute(gameRecord.getArc(), filter.getArc())
				&& this.checkFilterAttribute(gameRecord.getHyc(), filter.getHyc())
				&& this.checkFilterAttribute(gameRecord.getAyc(), filter.getAyc())
				&& this.checkFilterAttribute(gameRecord.gethPunti(), filter.gethPunti())
				&& this.checkFilterAttribute(gameRecord.getaPunti(), filter.getaPunti())
				&& this.checkFilterAttribute(gameRecord.getIp(), filter.getIp())
				&& this.checkFilterAttribute(gameRecord.getIpp(), filter.getIpp())
				&& this.checkFilterAttribute(gameRecord.getPossess()[0], filter.gethPossess())
				&& this.checkFilterAttribute(gameRecord.getPossess()[1], filter.getaPossess())
				&& this.checkFilterAttribute(gameRecord.getShot_on()[0], filter.gethShot_on())
				&& this.checkFilterAttribute(gameRecord.getShot_on()[1], filter.getaShot_on())
				&& this.checkFilterAttribute(gameRecord.getShot_off()[0], filter.gethShot_off())
				&& this.checkFilterAttribute(gameRecord.getShot_off()[1], filter.getaShot_off())
				&& this.checkFilterAttribute(this.sumUp(gameRecord.getShot_on()[0], gameRecord.getShot_on()[1]),
						filter.gettShot_on())
				&& this.checkFilterAttribute(this.sumUp(gameRecord.getShot_off()[0], gameRecord.getShot_off()[1]),
						filter.gettShot_off())
				&& this.checkFilterAttribute(this.sumUp(gameRecord.getHg(), gameRecord.getAg()), filter.gettGoal())
				&& this.checkFilterAttribute(
						this.sumUp(gameRecord.getDang_attacks()[0], gameRecord.getDang_attacks()[1]), filter.gettDatt())
				&& this.checkFilterAttribute(this.sumUp(gameRecord.getAttacks()[0], gameRecord.getAttacks()[1]),
						filter.gettAtt())
				&& this.checkFilterAttribute(gameRecord.getaPunti() + gameRecord.gethPunti(), filter.gettPunti())
				&& this.checkFilterAttribute(String.valueOf(Math.abs(
						Integer.valueOf(gameRecord.getPossess()[0]) - Integer.valueOf(gameRecord.getPossess()[1]))),
						filter.getDiffPoss());
	}

	private String sumUp(String s1, String s2) {
		return String.valueOf(Integer.valueOf(s1) + Integer.valueOf(s2));
	}

	private boolean checkFilterAttribute(Double value, GameFilterDoubleItem item) {
		return (item.getMin() == null || value >= item.getMin()) && (item.getMax() == null || value <= item.getMax());
	}

	/**
	 * Filter: Tesss1 Indonesia Liga 2 Persibat Batang - Perserang Serang Result:
	 * 0:0 Time: 49 IP: 2.51
	 * 
	 * @param gameRecord
	 * @param filter
	 * @return
	 */

	private String createMessage(Game gameRecord, GameFilterDto filter) {
		return "Filter: " + filter.getName() + "\n" + gameRecord.getL() + "\n" + gameRecord.getH() + " - "
				+ gameRecord.getA() + "\nResults: " + gameRecord.getHg() + ":" + gameRecord.getAg() + "\nTime: "
				+ gameRecord.getStatus() + "\nIP: " + gameRecord.getIp();
	}

	private boolean checkFilterAttribute(String value, GameFilterIntegerItem item) {
		return (item.getMin() == null || Integer.valueOf(value.equals("half") ? "45" : value) >= item.getMin())
				&& (item.getMax() == null || Integer.valueOf(value.equals("half") ? "45" : value) <= item.getMax());
	}

	@Scheduled(initialDelay = 1000, fixedRate = 20 * 1000)
	public void getGameStatistics() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("user-agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<GameStatisticResponseDto> response = restTemplate.exchange(this.gameStatUrl, HttpMethod.GET,
				entity, GameStatisticResponseDto.class);
		if (response.getStatusCode() == HttpStatus.OK) {
			this.logger.info("Received game statistic");
			GameStatisticResponseDto resp = response.getBody();
			int i = 0;
			if (resp.getData() != null) {
				for (Game game : resp.getData()) {
					game.setTimestamp(new Date());
					calculatePunti(game);
					calculateIP(game, null);
					this.calculateIP1(game);
					this.gameRepository.addGameRecord(game);
					this.saveGameInDb(game.getId());
				}
			} else {
				this.logger.warn("No game data received");
			}
		}
	}

	private void saveGameInDb(String id) {
		List<Game> gameRecords = this.gameRepository.getGames().get(id);
		if (gameRecords != null && gameRecords.size() >= 2) {
			int size = gameRecords.size();
			if (!gameRecords.get(size - 2).getHg().equals(gameRecords.get(size - 1).getHg())
					|| !gameRecords.get(size - 2).getAg().equals(gameRecords.get(size - 1).getAg())) {
				this.logger.info("Game save, because goal has fallen.");
				List<Game> forGames = this.gameRepository.getResultsForTimeAndGame(gameRecords, 45, "45");
				Game storedGame = forGames != null ? forGames.get(0) : gameRecords.get(size - 1);
				storedGame.setStoringType("GOAL");
				this.dbGameRepository.save(storedGame);
			}
		}
	}

	public static void calculateIP(Game game, Double divideWith) {
		int shotOff = Integer.valueOf(game.getShot_off()[0]);
		int shotOn = Integer.valueOf(game.getShot_on()[0]);
		double hShots;
		if (shotOff > 0) {
			hShots = Math.pow(shotOn + shotOff, 2);
		} else {
			hShots = 0.0;
		}

		shotOff = Integer.valueOf(game.getShot_off()[1]);
		shotOn = Integer.valueOf(game.getShot_on()[1]);
		double aShots;
		if (shotOff > 0) {
			aShots = Math.pow(shotOn + shotOff, 2);
		} else {
			aShots = 0.0;
		}
		int hc = Integer.valueOf(game.getHc());
		double hc1;
		if (hc > 0) {
			hc1 = Math.pow(hc, 2);
		} else {
			hc1 = 0.0;
		}
		int ac = Integer.valueOf(game.getAc());
		double ac1;
		if (ac > 0) {
			ac1 = Math.pow(ac, 2);
		} else {
			ac1 = 0.0;
		}
		int hDangAttacks = Integer.valueOf(game.getDang_attacks()[0]);
		int aDangAttacks = Integer.valueOf(game.getDang_attacks()[1]);
		double IP = ((hDangAttacks + aDangAttacks) + hShots + aShots + hc1 + ac1) / (divideWith != null
				? divideWith == 0 ? 1 : divideWith
				: Integer.valueOf(game.getStatus().equals("half") ? "45"
						: game.getStatus().equals("0") || game.getStatus().equals("00") ? "1" : game.getStatus()));
		game.setIp(round(IP));
		game.setIpp(round(game.gethPunti() > game.getaPunti() ? IP * game.gethPunti() : IP * game.getaPunti()));
	}

	public static double round(double value) {
		return Precision.round(value, 2);
	}

	private void calculateIP1(Game game) {
		double ip1H = Double.valueOf(game.getPossess()[0]) / 5 + Double.valueOf(game.getShot_on()[0])
				+ Double.valueOf(game.getShot_off()[0]);
		double ip1A = Double.valueOf(game.getPossess()[1]) / 5 + Double.valueOf(game.getShot_on()[1])
				+ Double.valueOf(game.getShot_off()[1]);
		game.setIp1H(round(ip1H));
		game.setIp1A(round(ip1A));

	}

	public static void calculatePunti(Game game) {
		double hPunti = 0;
		int dangAttacks = Integer.valueOf(game.getDang_attacks()[0]);
		int shotOn = Integer.valueOf(game.getShot_on()[0]);
		int shotOff = Integer.valueOf(game.getShot_off()[0]);
		int hc = Integer.valueOf(game.getHc());
		if (hc == 0) {
			hc = 1;
		}

		if (shotOn > 0 && shotOff > 0) {
			hPunti = (1.0 / 3.0) * dangAttacks * (shotOn + shotOff) / hc + Math.pow(shotOn, 2)
					+ shotOn * (shotOn / shotOff * 1 / hc - 1);
		}

		if (shotOn == 0 && shotOff > 0) {
			hPunti = (1.0 / 3.0) * dangAttacks * (shotOn + shotOff) / hc + shotOn * (shotOff * 1 / hc - 1);
		}

		if (shotOn > 0 && shotOff == 0) {
			hPunti = (1.0 / 3.0) * dangAttacks * shotOn / hc + Math.pow(shotOn, 2) + shotOn * (shotOn * 1 / hc - 1);
		}

		if (shotOn == 0 && shotOff == 0) {
			hPunti = 0.0;
		}
		double aPunti = 0;
		shotOn = Integer.valueOf(game.getShot_on()[1]);
		shotOff = Integer.valueOf(game.getShot_off()[1]);
		int ac = Integer.valueOf(game.getAc());
		dangAttacks = Integer.valueOf(game.getDang_attacks()[1]);
		if (ac == 0) {
			ac = 1;
		}
		if (shotOn > 0 && shotOff > 0) {
			aPunti = (1.0 / 3.0) * dangAttacks * (shotOn + shotOff) / ac + Math.pow(shotOn, 2)
					+ shotOn * (shotOn / shotOff * 1 / ac - 1);
		}

		if (shotOn == 0 && shotOff > 0) {
			aPunti = (1.0 / 3.0) * dangAttacks * (shotOn + shotOff) / ac + shotOn * (shotOff * 1 / ac - 1);
		}

		if (shotOn > 0 && shotOff == 0) {
			aPunti = (1.0 / 3.0) * dangAttacks * shotOn / ac + Math.pow(shotOn, 2) + shotOn * (shotOn * 1 / ac - 1);
		}

		if (shotOn == 0 && shotOff == 0) {
			aPunti = 0.0;
		}
		if (hPunti == 0) {
			hPunti = 1;
		}
		if (aPunti == 0) {
			aPunti = 1;
		}
		if (hPunti > aPunti) {
			game.sethPunti(round(hPunti / aPunti));
			game.setaPunti(round(aPunti / aPunti));
		} else {
			game.sethPunti(round(hPunti / hPunti));
			game.setaPunti(round(aPunti / hPunti));
		}
	}

}
